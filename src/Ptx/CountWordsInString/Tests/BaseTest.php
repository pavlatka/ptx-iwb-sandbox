<?php
declare(strict_types=1);

namespace Ptx\CountWordsInString\Tests;

abstract class BaseTest extends \PHPUnit\Framework\TestCase
{
    public function generateTestData() : array
    {
        return array(
            array('', 0),
            array(' ', 0),
            array('This is my first test case.', 6),
            array('This is my second " test case.', 6),
            array('I went to the job with Tim & Tom.', 8),
            array('I went to the job with Tim & Tom', 8)
        );
    }
}
