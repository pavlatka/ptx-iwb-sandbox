<?php
declare(strict_types=1);

namespace Ptx\CountWordsInString\Tests;

use Ptx\CountWordsInString\CountWordsInStringByStrWordCount;

class CountWordsInStringByStrWordCountTest extends BaseTest
{
    /**
     * @dataProvider generateTestData
     */
    public function testConstructCountNumberOfWordsProperly(
        string $string,
        int $expectedNumberOfWords
    ) {
        $solution = new CountWordsInStringByStrWordCount($string);
        $result   = $solution->getWordsCount();

        $this->assertEquals($expectedNumberOfWords, $result);
    }
}
