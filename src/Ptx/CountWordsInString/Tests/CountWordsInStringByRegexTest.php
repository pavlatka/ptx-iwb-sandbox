<?php
declare(strict_types=1);

namespace Ptx\CountWordsInString\Tests;

use Ptx\CountWordsInString\CountWordsInStringByRegex;

class CountWordsInStringByRegexTest extends BaseTest
{
    /**
     * @dataProvider generateTestData
     */
    public function testConstructCountNumberOfWordsProperly(
        string $string,
        int $expectedNumberOfWords
    ) {
        $solution = new CountWordsInStringByRegex($string);
        $result   = $solution->getWordsCount();

        $this->assertEquals($expectedNumberOfWords, $result);
    }
}
