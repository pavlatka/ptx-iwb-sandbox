<?php
declare(strict_types=1);

namespace Ptx\CountWordsInString\Tests;

use Ptx\CountWordsInString\CountWordsInStringByCheckingChars;

class CountWordsInStringByCheckingCharsTest extends BaseTest
{
    /**
     * @dataProvider generateTestData
     */
    public function testConstructCountNumberOfWordsProperly(
        string $string,
        int $expectedNumberOfWords
    ) {
        $solution = new CountWordsInStringByCheckingChars($string);
        $result   = $solution->getWordsCount();

        $this->assertEquals($expectedNumberOfWords, $result);
    }
}
