<?php declare(strict_types=1);

namespace Ptx\CountWordsInString;

abstract class BaseSolution
{
    protected /* int */    $wordsCount;
    protected /* string */ $stringIncludingWords;

    public function __construct(string $stringIncludingWords)
    {
        $this->stringIncludingWords = $stringIncludingWords;

        $this->countWordsInString();
    }

    public function getWordsCount() : int
    {
        return $this->wordsCount;
    }
}
