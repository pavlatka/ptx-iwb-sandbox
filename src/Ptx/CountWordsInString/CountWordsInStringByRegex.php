<?php declare(strict_types=1);

/**
 * To warm up, write a function that counts words in a string.
 * The string may include multiples of any type of non-word character,
 * such as space, tab, full stop, dash. Do this as efficiently as possible.
 */

namespace Ptx\CountWordsInString;

class CountWordsInStringByRegex extends BaseSolution
{
    protected function countWordsInString() : void
    {
        if (preg_match_all('/\w+/', $this->stringIncludingWords, $matches)) {
            $this->wordsCount = count($matches[0]);
        } else {
            $this->wordsCount = 0;
        }
    }
}
