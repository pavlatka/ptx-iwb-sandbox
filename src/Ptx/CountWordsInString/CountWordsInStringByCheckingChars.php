<?php declare(strict_types=1);

/**
 * To warm up, write a function that counts words in a string.
 * The string may include multiples of any type of non-word character,
 * such as space, tab, full stop, dash. Do this as efficiently as possible.
 * Write it without using any built-in functions,
 * but instead looking at each character of the string in sequence.
 */

namespace Ptx\CountWordsInString;

class CountWordsInStringByCheckingChars extends BaseSolution
{
    protected function countWordsInString() : void
    {
        $this->wordsCount = 0;

        $inWord       = false;
        $stringLength = strlen($this->stringIncludingWords);
        for ($i = 0; $i < $stringLength; $i++) {
            $ord = ord($this->stringIncludingWords[$i]);
            if ($this->isAWord($ord)) {
                $inWord = true;
            } elseif ($inWord) {
                $this->wordsCount++;
                $inWord = false;
            }
        }

        if ($inWord) {
            $this->wordsCount++;
        }
    }

    protected function isAWord(int $ord) : bool
    {
        if ($ord >= 48 && $ord <= 57) { // 0-9
            return true;
        } elseif ($ord >= 65 && $ord <= 90) { // A-Z
            return true;
        } elseif ($ord >= 97 && $ord <= 122) { // a-z
            return true;
        }

        return false;
    }
}
