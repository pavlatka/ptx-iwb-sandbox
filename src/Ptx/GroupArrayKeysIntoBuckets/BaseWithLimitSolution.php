<?php declare(strict_types=1);

namespace Ptx\GroupArrayKeysIntoBuckets;

abstract class BaseWithLimitSolution
{
    protected $limit;
    protected $groups;

    public function __construct(array $list, int $limit)
    {
        $this->list  = $list;
        $this->limit = $limit;

        $this->calculateGroups();
    }

    protected function addArray2Group(string $key, array $value) : void
    {
        if (!isset($this->groups[$key])) {
            $this->groups[$key] = array();
        }

        $this->groups[$key][] = $value;
    }

    protected function add2Group(string $key, string $value) : void
    {
        if (!isset($this->groups[$key])) {
            $this->groups[$key] = array();
        }

        $this->groups[$key][] = $value;
    }

    public function getList() : array
    {
        return $this->list;
    }

    public function getLimit() : int
    {
        return $this->limit;
    }

    public function getGroups() : array
    {
        return $this->groups;
    }
}
