<?php declare(strict_types=1);

namespace Ptx\GroupArrayKeysIntoBuckets\Tests;

use Ptx\GroupArrayKeysIntoBuckets\GroupKeysWithBucketGroupLimitArrayChunk;

class GroupKeysWithBucketGroupLimitArrayChunkTest extends BaseTest
{
    /**
     * @dataProvider generateTestDataWithLimit5GroupBucket
     */
    public function testConstructorThatGroupKeyCorrectly(array $list, int $limit, array $expectedGroups) : void
    {
        $result = new GroupKeysWithBucketGroupLimitArrayChunk($list, $limit);

        $this->assertEquals($expectedGroups, $result->getGroups());
    }
}
