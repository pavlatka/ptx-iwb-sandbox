<?php declare(strict_types=1);

namespace Ptx\GroupArrayKeysIntoBuckets\Tests;

use Ptx\GroupArrayKeysIntoBuckets\GroupKeysWithBucketGroupLimit;

class GroupKeysWithBucketGroupLimitTest extends BaseTest
{
    /**
     * @dataProvider generateTestDataWithLimit5GroupBucket
     */
    public function testConstructorThatGroupKeyCorrectly(array $list, int $limit, array $expectedGroups) : void
    {
        $result = new GroupKeysWithBucketGroupLimit($list, $limit);

        $this->assertEquals($expectedGroups, $result->getGroups());
    }
}
