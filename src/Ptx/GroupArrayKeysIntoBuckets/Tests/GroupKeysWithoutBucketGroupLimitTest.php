<?php declare(strict_types=1);

namespace Ptx\GroupArrayKeysIntoBuckets\Tests;

use Ptx\GroupArrayKeysIntoBuckets\GroupKeysWithoutBucketGroupLimit;

class GroupKeysWithoutBucketGroupLimitTest extends BaseTest
{
    /**
     * @dataProvider generateTestData
     */
    public function testConstructorThatGroupKeyCorrectly(array $list, array $expectedGroups) : void
    {
        $result = new GroupKeysWithoutBucketGroupLimit($list);

        $this->assertEquals($expectedGroups, $result->getGroups());
    }
}
