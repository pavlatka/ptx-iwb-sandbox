<?php declare(strict_types=1);

namespace Ptx\GroupArrayKeysIntoBuckets\Tests;

use Ptx\GroupArrayKeysIntoBuckets\GroupKeysWithoutBucketGroupLimitArrayWalk;

class GroupKeysWithoutBucketGroupLimitArrayWalkTest extends BaseTest
{
    /**
     * @dataProvider generateTestData
     */
    public function testConstructorThatGroupKeyCorrectly(array $list, array $expectedGroups) : void
    {
        $result = new GroupKeysWithoutBucketGroupLimitArrayWalk($list);

        $this->assertEquals($expectedGroups, $result->getGroups());
    }
}
