<?php declare(strict_types=1);

namespace Ptx\GroupArrayKeysIntoBuckets\Tests;

abstract class BaseTest extends \PHPUnit\Framework\TestCase
{
    protected $solution;

    public function generateTestData() : array
    {
        return array(
            array(
                array(
                    'key1'  => 'hello there very long string',
                    'key2'  => 'netsmart',
                    'key3'  => 'netsmart',
                    'key4'  => 'chips ahoy',
                    'key5'  => 'netsmart',
                    'key6'  => 'another long string imagine it\'s 20000 characters',
                    'key7'  => 'netsmart',
                    'key8'  => 'netsmart',
                    'key9'  => 'netsmart',
                    'key10' => 'chips ahoy',
                    'key11' => 'netsmart',
                ),
                array(
                    'hello there very long string' => array('key1'),
                    'netsmart'   => array('key2', 'key3', 'key5', 'key7', 'key8', 'key9', 'key11'),
                    'chips ahoy' => array('key4', 'key10'),
                    'another long string imagine it\'s 20000 characters' => array('key6')
                )
            )
        );
    }

    public function generateTestDataWithLimit5GroupBucket() : array
    {
        return array(
            array(
                array(
                    'key1'  => 'hello there very long string',
                    'key2'  => 'netsmart',
                    'key3'  => 'netsmart',
                    'key4'  => 'chips ahoy',
                    'key5'  => 'netsmart',
                    'key6'  => 'another long string imagine it\'s 20000 characters',
                    'key7'  => 'netsmart',
                    'key8'  => 'netsmart',
                    'key9'  => 'netsmart',
                    'key10' => 'chips ahoy',
                    'key11' => 'netsmart',
                ),
                5,
                array(
                    'hello there very long string' => array(array('key1')),
                    'netsmart' => array(
                        array('key2', 'key3', 'key5', 'key7', 'key8'),
                        array('key9', 'key11')
                    ),
                    'chips ahoy' => array(array('key4', 'key10')),
                    'another long string imagine it\'s 20000 characters' => array(array('key6'))
                )
            )
        );
    }
}
