<?php declare(strict_types=1);

/**
 * You have an randomly ordered associative array of $key => $data, where
 * $data is a long string **over 1000 chars long**.
 *
 * Write a function that transforms that into an array of buckets of $data, where keys with the same
 * $data are in a bucket.
 *
 * For example:
 *
 * $arr = array(
 *     'key1' => 'hello there very long string',
 *     'key2' => 'netsmart',
 *     'key3' => 'netsmart',
 *     'key4' => 'chips ahoy',
 *     'key5' => 'netsmart',
 *     'key6' => 'another long string imagine it\'s 20000 characters',
 *     'key7' => 'netsmart',
 *     'key8' => 'netsmart',
 *     'key9' => 'netsmart',
 *     'key10' => 'chips ahoy',
 *     'key11' => 'netsmart',
 * ) ;
 *
 * RESULT:
 *    array(
 *        'hello there very long string'  => array('key1'),
 *        'netsmart'   => array('key2', 'key3', 'key5', 'key7', 'key8', 'key9', 'key11'),
 *        'chips ahoy' => array('key4', 'key10'),
 *        'another long string imagine it\'s 20000 characters' => array('key6')
 *    )
 *
 */

namespace Ptx\GroupArrayKeysIntoBuckets;

class GroupKeysWithoutBucketGroupLimit extends BaseWithoutLimitSolution
{
    protected function calculateGroups() : void
    {
        foreach ($this->list as $key => $value) {
            $this->add2Group($value, $key);
        }
    }
}
