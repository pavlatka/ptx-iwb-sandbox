<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

$groupBucketLimit = 5;
$numberOfStrings  = 50000;
$measureSolution  = new Ptx\GroupArrayKeysIntoBuckets\Measure\MeasureSolution($numberOfStrings, $groupBucketLimit);

$timeTaken = $measureSolution->measureGroupKeysWithoutBucketGroupLimit();
echo '- ' . $numberOfStrings . ' by using GroupKeysWithoutBucketGroupLimit took: ';
echo  ($timeTaken * 1000) . ' ms' . "\r\n";

$timeTaken = $measureSolution->measureGroupKeysWithoutBucketGroupLimitArrayWalk($numberOfStrings);
echo '- ' . $numberOfStrings . ' by using GroupKeysWithoutBucketGroupLimitArrayWalk took: ';
echo ($timeTaken * 1000) . ' ms' . "\r\n";

$timeTaken = $measureSolution->measureGroupKeysWithBucketGroupLimit($numberOfStrings);
echo '- ' . $numberOfStrings . ' by using GroupKeysWithBucketGroupLimit took: ' ;
echo ($timeTaken * 1000) . ' ms' . "\r\n";

$timeTaken = $measureSolution->measureGroupKeysWithBucketGroupLimitArrayChunk($numberOfStrings);
echo '- ' . $numberOfStrings . ' by using GroupKeysWithBucketGroupLimitArrayChunk took: ';
echo ($timeTaken * 1000) . ' ms' . "\r\n";
