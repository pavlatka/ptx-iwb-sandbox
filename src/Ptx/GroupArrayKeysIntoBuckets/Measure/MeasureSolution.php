<?php declare(strict_types=1);

namespace Ptx\GroupArrayKeysIntoBuckets\Measure;

use Ptx\GroupArrayKeysIntoBuckets as B;

class MeasureSolution
{
    private $numberOfStrings;
    private $groupBucketLimit;
    private $listWithStrings = array();

    public function __construct(int $numberOfStrings, int $groupBucketLimit)
    {
        $this->numberOfStrings  = $numberOfStrings;
        $this->groupBucketLimit = $groupBucketLimit;

        $this->prepareListWithStrings();
    }

    public function measureGroupKeysWithoutBucketGroupLimit() : float
    {
        $start = microtime(true);

        $solution = new B\GroupKeysWithoutBucketGroupLimit($this->listWithStrings);
        $solution->getGroups();

        return microtime(true) - $start;
    }

    public function measureGroupKeysWithoutBucketGroupLimitArrayWalk() : float
    {
        $start = microtime(true);

        $solution = new B\GroupKeysWithoutBucketGroupLimitArrayWalk($this->listWithStrings);
        $solution->getGroups();

        return microtime(true) - $start;
    }

    public function measureGroupKeysWithBucketGroupLimit() : float
    {
        $start = microtime(true);

        $solution = new B\GroupKeysWithBucketGroupLimit(
            $this->listWithStrings,
            $this->groupBucketLimit
        );
        $solution->getGroups();

        return microtime(true) - $start;
    }

    public function measureGroupKeysWithBucketGroupLimitArrayChunk() : float
    {
        $start = microtime(true);

        $solution = new B\GroupKeysWithBucketGroupLimitArrayChunk(
            $this->listWithStrings,
            $this->groupBucketLimit
        );
        $solution->getGroups();

        return microtime(true) - $start;
    }

    protected function prepareListWithStrings() : void
    {
        for ($i = 0; $i < $this->numberOfStrings; $i++) {
            $randomString = $this->generateRandomString();
            $this->listWithStrings[$randomString] = 'key' . $i;
        }
    }

    protected function generateRandomString() : string
    {
        $string           = null;
        $characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);

        for ($i = 0; $i < mt_rand(1, 5); $i++) {
            $string .= $characters[rand(0, $charactersLength -1)];
        }

        $string .= mt_rand(1, 999999);

        return $string;
    }
}
