<?php declare(strict_types=1);

namespace Ptx\GroupArrayKeysIntoBuckets;

abstract class BaseWithoutLimitSolution
{
    protected $groups;

    public function __construct(array $list)
    {
        $this->list = $list;

        $this->calculateGroups();
    }

    protected function add2Group(string $key, string $value) : void
    {
        if (!isset($this->groups[$key])) {
            $this->groups[$key] = array();
        }

        $this->groups[$key][] = $value;
    }

    public function getList() : array
    {
        return $this->list;
    }

    public function getGroups() : array
    {
        return $this->groups;
    }
}
