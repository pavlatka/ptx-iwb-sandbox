<?php
declare(strict_types=1);

namespace Ptx\BinarySearch\Tests;

abstract class BaseTest extends \PHPUnit\Framework\TestCase
{
    public function generateTestData() : array
    {
        return array(
            array(array(), 1, -1),
            array(array(1), 1, 0),
            array(array(1,2,3,4,5,6,7,8,9,10), 5, 4),
            array(array(1,2,3,4,5,6,7,8,9,10), 1, 0),
            array(array(1,2,3,4,5,6,7,8,9,10), 10, 9),
            array(array(1,2,3,4,5,6,7,8,9,10), 15, -1),
            array(array(1,2,3,4,5,6,7,8,9,10), 0, -1),
        );
    }
}
