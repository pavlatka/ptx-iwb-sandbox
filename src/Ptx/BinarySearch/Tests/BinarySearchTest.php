<?php
declare(strict_types=1);

namespace Ptx\BinarySearch\Tests;

use Ptx\BinarySearch\BinarySearch;

class BinarySearchTest extends BaseTest
{
    /**
     * @dataProvider generateTestData
     */
    public function testBinarySearch(
        array $arrayToSearchIn,
        $searchedValue,
        int $expectedIndex
    ) {
        $solution = new BinarySearch($arrayToSearchIn, $searchedValue);
        $result   = $solution->getIndex();

        $this->assertEquals($expectedIndex, $result);
    }
}
