<?php declare(strict_types=1);

/**
 * Simple implemenation of the binary search in PHP
 */

namespace Ptx\BinarySearch;

class BinarySearch extends BaseSolution
{
	public function getIndex() : int
	{
		$leftIndex  = 0;
		$rightIndex = count($this->arrayToSearchIn) - 1;

		while ($leftIndex <= $rightIndex) {
			$middle  = (int) ((($rightIndex - $leftIndex) / 2) + $leftIndex);
			$compare = $this->arrayToSearchIn[$middle] <=> $this->searchedValue;

			if ($compare < 0) {
				$leftIndex = $middle + 1;
			} else if ($compare > 0) {
				$rightIndex = $middle - 1;
			} else {
				return $middle;
			}
		}

		return -1;
	}
}
