<?php declare(strict_types=1);

namespace Ptx\BinarySearch;

abstract class BaseSolution
{
    protected $searchedValue;
    protected $arrayToSearchIn;

    public function __construct(array $arrayToSearchIn, $searchedValue)
    {
        $this->searchedValue   = $searchedValue;
        $this->arrayToSearchIn = $arrayToSearchIn;
    }
}
