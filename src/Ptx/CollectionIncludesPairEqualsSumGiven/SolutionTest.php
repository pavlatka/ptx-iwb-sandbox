<?php
declare(strict_types=1);

namespace Ptx\CollectionIncludesPairEqualsSumGiven;

class SolutionTest extends \PHPUnit\Framework\TestCase
{
    private $solution;

    protected function setUp()
    {
        $this->solution = new Solution();
    }

    protected function tearDown()
    {
        unset($this->solution);
    }

    /**
     * @dataProvider generateTestDataOrdered
     * @dataProvider generateTestDataUnOrdered
     */
    public function testSolveBy2ForLoops(array $list, int $sum, bool $expected)
    {
        $result = $this->solution->solveBy2ForLoops($list, $sum);

        $this->compareResult($result, $expected);
    }

    /**
     * @dataProvider generateTestDataOrdered
     * @dataProvider generateTestDataUnOrdered
     */
    public function testSolveByArrayDiffKey(array $list, int $sum, bool $expected)
    {
        $result = $this->solution->solveByArrayDiffKey($list, $sum);

        $this->compareResult($result, $expected);
    }

    /**
     * @dataProvider generateTestDataOrdered
     */
    public function testSolveByMovingCursor(array $list, int $sum, bool $expected)
    {
        $result = $this->solution->solveByMovingCursor($list, $sum);

        $this->compareResult($result, $expected);
    }

    /**
     * @dataProvider generateTestDataOrdered
     * @dataProvider generateTestDataUnOrdered
     */
    public function testSolveByComplementsArray(array $list, int $sum, bool $expected)
    {
        $result = $this->solution->solveByComplementsArray($list, $sum);

        $this->compareResult($result, $expected);
    }

    protected function compareResult(bool $result, bool $expected)
    {
        if ($expected) {
            $this->assertTrue($result);
        } else {
            $this->assertFalse($result);
        }
    }

    public function generateTestDataOrdered() : array
    {
        return array(
            array(
                array(1, 2, 3, 9),
                8,
                false
            ),
            array(
                array(1, 2, 4, 4),
                8,
                true
            ),
            array(
                array(1, 2, 4, 4, 5, 6, 10, 12, 15),
                27,
                true
            ),
            array(
                array(1, 2, 4, 4, 5, 6, 10, 12, 15),
                11,
                true
            ),
            array(
                array(1, 2, 4, 4, 5, 6, 10, 12, 15, 16, 22, 34, 122, 123),
                255,
                false
            ),
            array(
                array(1, 2, 4, 4, 5, 6, 10, 12, 15, 16, 22, 34, 122, 133),
                255,
                true
            ),
        );
    }

    public function generateTestDataUnOrdered() : array
    {
        return array(
            array(
                array(1, 2, 3, 9, 5),
                8,
                true
            ),
            array(
                array(1, 2, 4, 3, 2),
                8,
                false
            ),
            array(
                array(1, 2, 4, 10, 12, 15, 4, 5, 6),
                27,
                true
            ),
            array(
                array(1, 2, 4, 10, 12, 15, 4, 5, 6),
                11,
                true
            ),
            array(
                array(1, 2, 4, 4, 15, 16, 22, 34, 122, 123, 5, 6, 10, 12),
                255,
                false
            ),
            array(
                array(1, 2, 4, 4, 16, 22, 34, 122, 133, 5, 6, 10, 12, 15),
                255,
                true
            ),
        );
    }
}
