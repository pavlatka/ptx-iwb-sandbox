<?php declare(strict_types=1);

namespace Ptx\CollectionIncludesPairEqualsSumGiven;

/**
 * Idea is that you will recieve 2 variables
 * 1. Array of the integers
 * 2. Sum
 *
 * The tasks is to find whether the array given contains a pair which sum would be equal to
 * sum given.
 *
 * Example 1:
 * - array : [1,2,3,9]
 * - sum : 8
 * - result : false
 *
 * Example 2:
 * - array : [1,2,4,4]
 * - sum : 8
 * - result : true ( 4 + 4 = 8 )
 */

class Solution
{
    public function solveBy2ForLoops(array $list, int $sum) : bool
    {
        $listSize = count($list);
        if ($listSize == 0) {
            return false;
        }

        for ($i = 0; $i < $listSize; $i++) {
            for ($j = 0; $j < $listSize; $j++) {
                if ($i != $j && ($list[$i] + $list[$j]) == $sum) {
                    return true;
                }
            }
        }

        return false;
    }

    public function solveByArrayDiffKey(array $list, int $sum) : bool
    {
        foreach ($list as $key => $record) {
            $complement       = $sum - $record;
            $complementsArray = array_diff_key($list, array($key => $record));

            if (in_array($complement, $complementsArray, true)) {
                return true;
            }
        }

        return false;
    }

    /**
     * This solution is good in case when we can ensure that the data
     * in the $list are sorted from smaller to greater
     */
    public function solveByMovingCursor(array $list, int $sum) : bool
    {
        $min = 0;
        $max = count($list) - 1;

        while ($min < $max) {
            $checkSum = $list[$min] + $list[$max];

            if ($checkSum == $sum) {
                return true;
            } elseif ($checkSum > $sum) {
                --$max;
            } else {
                ++$min;
            }
        }

        return false;
    }

    public function solveByComplementsArray(array $list, int $sum) : bool
    {
        $complementsArray = array();

        foreach ($list as $record) {
            if (in_array($record, $complementsArray, true)) {
                return true;
            }

            $complement         = $sum - $record;
            $complementsArray[] = $complement;
        }

        return false;
    }
}
