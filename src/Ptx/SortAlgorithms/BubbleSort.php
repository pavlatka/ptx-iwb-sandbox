<?php
declare(strict_types=1);

/**
 * Simple implemenation of the bubble sort in PHP
 */

namespace Ptx\SortAlgorithms;

class BubbleSort extends BaseSolution
{
	public function getSortedList() : array
	{
        $sortedList = $this->listOfItems;
        $listCount  = count($sortedList);

        do {
            $swapped = false;
            for ($i = 1; $i < $listCount; $i++) {
                $previousKey  = $i - 1;
                $value        = $sortedList[$i];
                $compareValue = $sortedList[$previousKey];

                if ($value < $compareValue) {
                    $sortedList[$i]           = $compareValue;
                    $sortedList[$previousKey] = $value;
                    $swapped                  = true;
                }
            }
        } while ($swapped);

        return $sortedList;
	}
}
