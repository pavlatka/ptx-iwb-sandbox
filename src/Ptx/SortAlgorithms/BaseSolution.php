<?php
declare(strict_types=1);

namespace Ptx\SortAlgorithms;

abstract class BaseSolution
{
    protected $listOfItems;

    public function __construct(array $listOfItems)
    {
       $this->listOfItems = $listOfItems;
    }
}
