<?php
declare(strict_types=1);

namespace Ptx\SortAlgorithms\Tests;

use Ptx\SortAlgorithms\BubbleSort;

class BubbleSortTest extends BaseTest
{
    /**
     * @dataProvider generateTestData
     */
    public function testBubbleSort(
        array $listOfItems,
        array $expectedResult
    ) {
        $solution = new BubbleSort($listOfItems);
        $result   = $solution->getSortedList();

        $this->assertEquals($expectedResult, $result);
    }
}
