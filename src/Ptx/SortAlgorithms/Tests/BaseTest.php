<?php
declare(strict_types=1);

namespace Ptx\SortAlgorithms\Tests;

abstract class BaseTest extends \PHPUnit\Framework\TestCase
{
    public function generateTestData() : array
    {
        return array(
            array(array(5, 1, 2, 4, 8), array(1, 2, 4, 5, 8)),
            array(array(5, 1, 2, 4, 8, 2, 18, 29, 10), array(1, 2, 2, 4, 5, 8, 10, 18, 29)),
            array(array(1, 1, 2, 3, 8, 2, 18, 29, 10, 123, 111, 1000), array(1, 1, 2, 2, 3, 8, 10, 18, 29, 111, 123, 1000))
        );
    }
}
