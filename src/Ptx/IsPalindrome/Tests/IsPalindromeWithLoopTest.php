<?php
declare(strict_types=1);

namespace Ptx\IsPalindrome\Tests;

use Ptx\IsPalindrome\IsPalindromeWithLoop;

class IsPalindromeWithLoopTest extends BaseTest
{
    /**
     * @dataProvider generateTestData
     */
    public function testIsPalindrome(
        string $string,
        bool $expectedResult
    ) {
        $solution = new IsPalindromeWithLoop($string);
        $result   = $solution->isPalindrome();

        if ($expectedResult) {
            $this->assertTrue($result);
        } else {
            $this->assertFalse($result);
        }
    }
}
