<?php
declare(strict_types=1);

namespace Ptx\IsPalindrome\Tests;

use Ptx\IsPalindrome\IsPalindromeRecursiveFunction;

class IsPalindromeRecursiveFunctionTest extends BaseTest
{
    /**
     * @dataProvider generateTestData
     */
    public function testIsPalindrome(
        string $string,
        bool $expectedResult
    ) {
        $solution = new IsPalindromeRecursiveFunction($string);
        $result   = $solution->isPalindrome();

        if ($expectedResult) {
            $this->assertTrue($result);
        } else {
            $this->assertFalse($result);
        }
    }
}
