<?php
declare(strict_types=1);

namespace Ptx\IsPalindrome\Tests;

abstract class BaseTest extends \PHPUnit\Framework\TestCase
{
    public function generateTestData() : array
    {
        return array(
            array('racecar', true),
            array('tomas', false),
            array('a', true),
            array('Now saw ye no mosses or foam, or aroma of roses. So money was won.', true),
        );
    }
}
