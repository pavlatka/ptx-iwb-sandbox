<?php
declare(strict_types=1);

namespace Ptx\IsPalindrome;

class IsPalindromeRecursiveFunction extends BaseSolution
{
    public function isPalindrome() : bool
    {
        return $this->isPalindromeRecursive($this->adept);
    }

    private function isPalindromeRecursive(?string $string)
    {
        $stringLength = mb_strlen($string);
        if ($stringLength <= 1) {
            return true;
        }

        $firstLetter = $string[0];
        $lastLetter  = $string[$stringLength - 1];

        if ($firstLetter != $lastLetter) {
            return false;
        }

        return $this->isPalindromeRecursive(substr($string, 1, -1));
    }
}
