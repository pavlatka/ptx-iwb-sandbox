<?php declare(strict_types=1);

namespace Ptx\IsPalindrome;

abstract class BaseSolution
{
    protected $adept;

    public function __construct(string $adept)
    {
        $this->prepareAdept4Decision($adept);
    }

    protected function prepareAdept4Decision(string $adept)
    {
        $adept = strtolower(trim($adept));
        $adept = preg_match_all('/[a-z]+/', $adept, $matches);

        $this->adept = implode('', $matches[0]);
    }
}
