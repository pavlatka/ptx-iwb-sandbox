<?php
declare(strict_types=1);

namespace Ptx\IsPalindrome;

class IsPalindromeWithLoop extends BaseSolution
{
    public function isPalindrome() : bool
    {
        $stringLength = mb_strlen($this->adept);
        $lastIndex    = $stringLength - 1;

        for ($i = 0; $i < $stringLength; $i++) {
            if ($this->adept[$i] != $this->adept[$lastIndex - $i]) {
                return false;
            }
        }

        return true;
    }
}
